# [Ludibrium](https://gitlab.com/eidoom/ludibrium)

[Live](https://eidoom.gitlab.io/ludibrium)

[Etymology](https://en.wikipedia.org/wiki/Ludibrium)

TODO: add grace period/truce at start for n turns to avoid the "second player always usurps" strategy with two players.
