'use strict';

const canvas = document.getElementById("canvas");
canvas.width = 720;
canvas.height = canvas.width;

const ctx = canvas.getContext("2d", {
    alpha: false,
});

const pl = document.getElementById("players");
const si = document.getElementById("size");
const player_colours = ['maroon', 'olive', 'green', 'teal', 'navy', 'purple', 'red', 'yellow', 'lime', 'cyan', 'blue', 'magenta'];
pl.max = player_colours.length;
const colours = ['black'].concat(player_colours);

let players;

const m = document.getElementById("majority");
const mv = document.getElementById("moves");
const s = document.getElementById("scores");
const r = document.getElementById("reset");
const u = document.getElementById("undo");

let ps = []
ps.init = num => {
    s.replaceChildren();
    ps.length = num;
    for (let i = 0; i < num; i++) {
        const p = document.createElement("span");
        ps[i] = s.appendChild(p);
        ps[i].style.color = player_colours[i];
    }
};
ps.update = scores_ => {
    for (let i = 0; i < ps.length; i++) {
        ps[i].textContent = player_colours[i] + ": " + scores_[i];
    };
};

let scores = [];
scores.update = state_ => {
    scores.fill(0);
    state_.data.forEach(i => {
        if (i !== 0) {
            scores[i - 1]++;
        }
    });
};

const t = document.getElementById("turn");
t.set_turn = i => {
    t.textContent = player_colours[i] + "'s turn";
    t.style.color = player_colours[i];
};

let board = {};
board.init = (num, players_) => {
    board.width = num;
    board.height = num;
    board.size = board.width * board.height;
    board.majority = Math.floor(board.size / players_);
};

const tile = {
    margin: 2,
};
tile.init = (canvas_, board_) => {
    tile.width = canvas_.width / board_.width;
    tile.height = canvas_.height / board_.height;
    tile.inner_width = tile.width - 2 * tile.margin;
    tile.inner_height = tile.height - 2 * tile.margin;
};

class State {
    data;
    width;

    constructor(b) {
        this.data = Array(b.size);
        this.width = b.width;
    }

    reset() {
        this.data.fill(0);
    }

    clone(other) {
        this.data = other.data.slice();
    }

    get(x, y) {
        return this.data[y * this.width + x];
    }

    set(x, y, v) {
        this.data[y * this.width + x] = v;
    }
}

let history;
let done;

function bounds(obj) {
    const min = parseInt(obj.min);
    const max = parseInt(obj.max);
    const val = parseInt(obj.value);
    if (min <= val && max >= val) {
        obj.style.color = "black";
        // nb assumes square board
        if (pl.value !== players || si.value !== board.width) {
            r.disabled = false;
        } else {
            r.disabled = true;
        }
    } else {
        obj.style.color = "red";
        r.disabled = true;
    }
}

si.addEventListener("input", event => {
    bounds(si);
});

pl.addEventListener("input", event => {
    bounds(pl);
});

reset.addEventListener("click", event => {
    if (!history.length || window.confirm("Are you sure you wish to restart the game?")) {
        init();
    }
});

undo.addEventListener("click", event => {
    if (history.length) {
        state.data = history.pop();

        mv.textContent = history.length;
        if (!history.length) {
            u.disabled = true;
            // nb assumes square board
            if (pl.value === players && si.value === board.width) {
                r.disabled = true;
            }
        }

        scores.update(state);

        ps.update(scores);

        const n = history.length % players;

        t.set_turn(n);

        if (done) {
            done = false;
            t.style.fontWeight = "normal";
        }

        window.requestAnimationFrame(render);
    }
});

canvas.addEventListener("click", event => {
    if (!done) {
        const p = (history.length % players) + 1;

        history.push(state.data.slice());

        mv.textContent = history.length;
        u.disabled = false;
        r.disabled = false;

        const rect = canvas.getBoundingClientRect();
        const scale_x = canvas.width / rect.width;
        const scale_y = canvas.height / rect.height;
        const x = (event.clientX - rect.x) * scale_x;
        const y = (event.clientY - rect.y) * scale_y;
        const X = Math.floor(x / tile.width);
        const Y = Math.floor(y / tile.height);

        state.set(X, Y, p);
        let freeze = new State(board);
        freeze.clone(state);

        for (let yy = 0; yy < board.height; yy++) {
            for (let xx = 0; xx < board.width; xx++) {
                const pp = freeze.get(xx, yy);
                if (pp === p) {
                    if ((xx > 1) && (freeze.get(xx - 2, yy) === pp)) {
                        state.set(xx - 1, yy, pp);
                    }
                    if ((yy > 1) && (freeze.get(xx, yy - 2) === pp)) {
                        state.set(xx, yy - 1, pp);
                    }
                }
            }
        }

        scores.update(state);

        ps.update(scores);

        const n = history.length % players;
        t.set_turn(n);

        for (let i = 0; i < players; i++) {
            if (scores[i] > board.majority) {
                t.textContent = player_colours[i] + " won!";
                t.style.color = player_colours[i];
                t.style.fontWeight = "bold";
                done = true;
            }
        }

        window.requestAnimationFrame(render);
    }
});

function render() {
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    for (let y = 0; y < board.height; y++) {
        for (let x = 0; x < board.width; x++) {
            ctx.fillStyle = colours[state.get(x, y)];
            ctx.fillRect(x * tile.width + tile.margin, y * tile.height + tile.margin, tile.inner_width, tile.inner_height);
        }
    }
};

let state;

function init() {
    players = pl.value;
    board.init(si.value, players);
    tile.init(canvas, board);
    state = new State(board);
    history = [];
    mv.textContent = history.length;
    u.disabled = true;
    r.disabled = true;
    ps.init(players);
    scores.length = players;
    m.textContent = 1 + board.majority;
    done = false;
    t.style.fontWeight = "normal";
    scores.fill(0);
    state.reset();
    t.set_turn(history.length);
    ps.update(scores);
    window.requestAnimationFrame(render);
}

document.onload = init();
